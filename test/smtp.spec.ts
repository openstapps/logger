/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {expect} from 'chai';
import {suite, test} from '@testdeck/mocha';
import {SMTP} from '../src/smtp';

@suite()
export class SMTPSpec {
  /* tslint:disable:member-ordering */
  @test
  mailValidation1() {
    expect(SMTP.isValidEmailAddress('stordeur@campus.tu-berlin.de')).to.be.true;
  }

  @test
  mailValidation2() {
    expect(SMTP.isValidEmailAddress('foo@bar.com')).to.be.true;
  }

  @test
  mailValidation3() {
    expect(SMTP.isValidEmailAddress('test@test.cz')).to.be.true;
  }

  @test
  mailValidation4() {
    expect(SMTP.isValidEmailAddress('info@beispiel.to')).to.be.true;
  }

  @test
  mailValidation5() {
    expect(SMTP.isValidEmailAddress('stördeur@campus.tu-berlin.de')).to.be.true;
  }

  @test
  mailValidation6() {
    expect(SMTP.isValidEmailAddress('stordeur@campus.tu-berlin.de+a')).to.be.true;
  }

  @test
  mailValidation7() {
    expect(SMTP.isValidEmailAddress('stordeurcampus.tu-berlin.de')).to.be.false;
  }

  @test
  mailValidation8() {
    expect(SMTP.isValidEmailAddress('@campus.tu-berlin.de')).to.be.false;
  }

  @test
  mailValidation9() {
    expect(SMTP.isValidEmailAddress('')).to.be.false;
  }

  @test
  mailValidation10() {
    expect(SMTP.isValidEmailAddress('@')).to.be.false;
  }

  @test
  mailValidation11() {
    expect(SMTP.isValidEmailAddress('@')).to.be.false;
  }

  @test
  mailValidation12() {
    expect(SMTP.isValidEmailAddress(' stordeur@campus.tu-berlin.de')).to.be.false;
  }

  @test
  mailValidation13() {
    expect(SMTP.isValidEmailAddress('stordeur@campus.tu-berlin.de ')).to.be.false;
  }

  @test
  mailValidation14() {
    expect(SMTP.isValidEmailAddress('stord+eur@campus.tu-berlin.de ')).to.be.false;
  }

  @test
  mailValidation15() {
    expect(SMTP.isValidEmailAddress('anselm..stordeur@campus.tu-berlin.de')).to.be.false;
  }

  @test
  mailValidation16() {
    expect(SMTP.isValidEmailAddress('stordeur@campus..tu-berlin.de')).to.be.false;
  }

  @test
  mailValidation17() {
    expect(SMTP.isValidEmailAddress('stordeur@campus')).to.be.false;
  }

  /* tslint:enable:member-ordering */
}
