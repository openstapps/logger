/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {expect} from 'chai';
import {suite, test} from '@testdeck/mocha';
import {
  deleteUndefinedProperties,
  isNodeEnvironment,
  isProductiveEnvironment,
  isProductiveNodeEnvironment,
} from '../src/common';

@suite()
export class CommonSpec {
  @test
  deleteUndefinedProperties1() {
    expect(deleteUndefinedProperties(
      {
        a: 2,
        b: {
          c: 3,
          d: undefined,
        },
      },
    )).to.deep.equal(
      {
        a: 2,
        b: {
          c: 3,
        },
      },
    );
  }

  @test
  deleteUndefinedProperties2() {
    expect(deleteUndefinedProperties(
      {
        a: undefined,
        b: undefined,
      },
    )).to.deep.equal(
      {},
    );
  }

  @test
  deleteUndefinedProperties3() {
    expect(deleteUndefinedProperties(
      {
        a: 2,
        b: 'foo',
        c: 'bar',
      },
    )).to.deep.equal(
      {
        a: 2,
        b: 'foo',
        c: 'bar',
      },
    );
  }

  @test
  isNodeEnvironment() {
    expect(isNodeEnvironment()).to.be.equal(true);

    const savedProcess = process;

    // @ts-ignore
    process = undefined;

    expect(isNodeEnvironment()).to.be.equal(false);

    process = savedProcess;
  }

  @test
  isProductiveEnvironment() {
    const nodeEnv = process.env.NODE_ENV;

    process.env.NODE_ENV = '';

    expect(isProductiveEnvironment()).to.be.equal(false);

    process.env.NODE_ENV = 'production';

    expect(isProductiveEnvironment()).to.be.equal(true);

    process.env.NODE_ENV = nodeEnv;
  }

  @test
  isProductiveNodeEnvironment() {
    const nodeEnv = process.env.NODE_ENV;

    process.env.NODE_ENV = '';

    expect(isProductiveNodeEnvironment()).to.be.equal(false);

    process.env.NODE_ENV = 'production';

    expect(isProductiveNodeEnvironment()).to.be.equal(true);

    process.env.NODE_ENV = nodeEnv;
  }
}
