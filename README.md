# @openstapps/logger

[![pipeline status](https://gitlab.com/openstapps/logger/badges/master/pipeline.svg)](https://gitlab.com/openstapps/logger/commits/master)
[![coverage report](https://gitlab.com/openstapps/logger/badges/master/coverage.svg)](https://gitlab.com/openstapps/logger/commits/master)

This is a simple logger for TypeScript projects with transformations for the log output.

Logs are only printed if their log level is equal or higher than the defined log level.

## Log Levels

Available log levels are:
- 1 - INFO
- 2 - LOG
- 4 - WARN
- 8 - ERROR
- 16 - OK

You can set your log level with the environment variable `STAPPS_LOG_LEVEL`.

To select your desired log levels add the corresponding numbers and set the value of `STAPPS_LOG_LEVEL` to the sum.

For example `STAPPS_LOG_LEVEL=17` is 16 + 1 and would log everything that is `OK` or `INFO`.

If you want to use logger in production (`NODE_ENV=production`) and allow all transports to fail set `ALLOW_NO_TRANSPORT` to `true`.

Additionally setting the environment variable `STAPPS_EXIT_LEVEL` which works in the same manner as `STAPPS_LOG_LEVEL` will terminate your process after logging at the selected level(s) (usefull for integration tests). It will be ignored in afore mentioned productive environments.

## SMTP

This class also provides a simple implementation of an SMTP transport which can be used as a
`TransportWithVerification` for the logger. You can use this to transport errors of the logger or to transport mails
of your own monitoring solution.

### Usage

You can instatiate it with a config or it will check for a config in the environment variables. Environment variables
can overwrite the actual config values.

Environment variables are:

* SMTP_AUTH_USER:      SMTP username
* SMTP_AUTH_PASSWORD:  SMTP password
* SMTP_HOST:           SMTP host
* SMTP_PORT:           SMTP port
* SMTP_RECIPIENTS:     comma seperated list of recipients
* SMTP_CC:             comma seperated list of recipients for the carbon copy (CC)
* SMTP_SENDER_MAIL:    sender of the mail
* SMTP_SENDER_NAME:    name of the sender
* SMTP_SECURE:         `true` to enable tls

## Transformations

By default the logger will only add the log level to the message. It will replace newlines with spaces and might skip some of your choosen Transformers when in production (`NODE_ENV=production`) for compatibility reasons with existing log aggregators and analyzers.

You can change this behavior by setting other Transformers via `Logger.setTransformations`. If you do so, mind the order of the transformers.

You may choose from the following:

* `AddLogLevel`, which prepends the output with the log level
* `Colorize`, which colorizes the output according to the log level
* `Timestamp`, which prepends a timestamp to the output
