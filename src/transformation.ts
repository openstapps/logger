/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {LogLevel} from './logger';

/**
 * A transformer for log output
 */
export interface Transformation {
  /**
   * Indicates if this transformation is stripped in production environments
   */
  useInProduction: boolean;

  /**
   * Transform an output
   *
   * @param logLevel Log level to transform output for
   * @param output Output to transform
   */
  transform(logLevel: LogLevel, output: string): string;
}
